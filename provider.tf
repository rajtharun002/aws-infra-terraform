terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region  = var.region
  profile = var.profile
}


# Configure S3 backend with Dynamo DB state lock

# terraform {
#   backend "s3" {
#     bucket         = "bucket_name"
#     key            = "backend/terraform.tfstate"
#     region         = "region"
#     dynamodb_table = "dynamo_db_table_name"
#   }
# }

