output "vpc_id" {
    value = module.vpc.vpc_id
}

output "cluster_id" {
    value = module.eks.cluster_id
}

output "cluster_arn" {
    value = module.eks.cluster_arn
}

output "cluster_certificate_authority_data" {
    value = module.eks.cluster_certificate_authority_data
    sensitive = true
}

output "cluster_endpoint" {
    value = module.eks.cluster_endpoint
}

output "aws_iam_openid_connect_provider_arn" {
    value = module.eks.aws_iam_openid_connect_provider_arn
}

output "aws_iam_openid_connect_provider_extract_from_arn" {
    value = module.eks.aws_iam_openid_connect_provider_extract_from_arn
}

output "bastion_IP" {
  value = module.ec2.basiton_IP
}

output "eks_nodegroup_role_name" {
  value = module.eks.eks_nodegroup_role_name
}

data "aws_caller_identity" "current" {}
output "account_id" {
  value = data.aws_caller_identity.current.account_id
}

output "terraform_eks_codebuild_kubectl_role" {
    value = module.codepipeline.terraform_eks_codebuild_kubectl_role
}

# output "clone_url" {
#     value = module.codepipeline.clone_url
# }

output "bastion_host_id" {
    value = module.ec2.bastion_host_id
}

output "doodlebluepoc_acm_cert_arn" {
  value = module.acm.doodlebluepoc_acm_cert_arn
}
