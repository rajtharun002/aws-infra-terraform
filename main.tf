module "vpc" {
  region       = var.region
  project-name = var.project-name

  source = "./modules/vpc/"

}

module "eks" {
  depends_on = [
    module.vpc
  ]
  project-name         = var.project-name
  private_subnet_01_id = module.vpc.private_subnet_01_id
  private_subnet_02_id = module.vpc.private_subnet_02_id
  source               = "./modules/eks/"
}

module "ec2" {
  depends_on = [
    module.vpc
  ]
  project-name        = var.project-name
  vpc_id              = module.vpc.vpc_id
  public_subnet_01_id = module.vpc.public_subnet_01_id
  source              = "./modules/ec2"

}

# module "iam" {
#   project-name = var.project-name
#   source = "./modules/iam"

# }

module "codepipeline" {
  depends_on = [
    module.vpc,
    module.ec2,
    module.eks
  ]
  project-name        = var.project-name
  source           = "./modules/codepipeline"
  eks_cluster_name = module.eks.cluster_id
}

module "acm" {
  project-name        = var.project-name
  source = "./modules/acm"

}

module "cloudtrail" {
  project-name = var.project-name
  source       = "./modules/cloudTrail"

}

module "cloudwatch" {
  project-name    = var.project-name
  bastion_host_id = module.ec2.bastion_host_id
  source          = "./modules/cloudwatch"

}

module "vpc_flow_log" {
  project-name = var.project-name
  vpc_id       = module.vpc.vpc_id
  source       = "./modules/vpcFlowLogs"

}
