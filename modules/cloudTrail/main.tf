####################################
#Define Caller Identity (To know which user is calling.. [Account ID])
####################################


data "aws_caller_identity" "current" {}


####################################
#Define S3 bucket
####################################

resource "aws_s3_bucket" "cloudtrail_bucket" {
  bucket        = "${var.project-name}-cloudtrail-bucket-${data.aws_caller_identity.current.account_id}"
  force_destroy = true 
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.cloudtrail_bucket.id
  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "${aws_s3_bucket.cloudtrail_bucket.arn}"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "${aws_s3_bucket.cloudtrail_bucket.arn}/AWSLogs/${data.aws_caller_identity.current.account_id}/*"
          }
    ]
  }
  EOF
}

#####################################
#Define Cloudtrail
#####################################

resource "aws_cloudtrail" "cloudtrail" {
  name           = "${var.project-name}-cloudtrail"
  s3_bucket_name = aws_s3_bucket.cloudtrail_bucket.id

  include_global_service_events = true
} 
