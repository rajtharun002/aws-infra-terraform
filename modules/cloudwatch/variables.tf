variable "project-name" {
  description = "Project Name"
  type        = string
  default     = ""
}

variable "service_names" {
  type = list(string)
  default = [
    "booking_service",
    "user_service"
  ]
}

variable "cpu_alarm_topic_arn" {
  type    = string
  default = ""
}

variable "bastion_host_id" {
  type    = string
  default = ""
}

variable "alarm_description" {
  default = "This metric monitors sanchu bastion host cpu utilization"
}

variable "threshold" {
  default = "80"
}

variable "statistics" {
  type = map(string)
  default = {
    "sc" : "SampleCount",
    "avg" : "Average",
    "sum" : "Sum",
    "min" : "Minimum",
    "max" : "Maximum"
  }
}

variable "period" {
  type        = string
  description = "The period in seconds over which the specified statistic is applied"
  default     = "300"
}

variable "evaluation_periods" {
  type        = string
  description = "Number of datapoints within the evaluation period that must be breaching to cause the alarm to go to ALARM state."
  default     = "1"
}

variable "comparison_operators" {
  type = map(string)
  default = {
    "GTOE" = "GreaterThanOrEqualToThreshold",
    "GT"   = "GreaterThanThreshold",
    "LT"   = "LessThanThreshold",
    "LTOE" = "LessThanOrEqualToThreshold"

  }
}

variable "metric_names" {
  type = map(string)
  default = {
    cpu : "CPUUtilization"
  }
}

variable "namespaces" {
  type = map(string)
  default = {
    "ec2" = "AWS/EC2"
  }
}