#####################################
# Define Log Groups
#####################################

resource "aws_cloudwatch_log_group" "yada" {
  count = length(var.service_names)
  name  = element(var.service_names, count.index)

  tags = {
    Environment = "production"
    Service     = element(var.service_names, count.index)
  }
}

#####################################
# Define CPU ALARM
#####################################

# resource "aws_cloudwatch_metric_alarm" "cpu_alarm" {
#   alarm_name                = "bastion_cpu_util_alarm"
#   comparison_operator       = var.comparison_operators.GTOE
#   period                    = var.period
#   evaluation_periods        = var.evaluation_periods
#   metric_name               = var.metric_names.cpu
#   namespace                 = var.namespaces.ec2
#   statistic                 = var.statistics.avg
#   threshold                 = var.threshold
#   alarm_description         = var.alarm_description
#   insufficient_data_actions = []
#   alarm_actions             = [var.cpu_alarm_topic_arn]
#   dimensions = {
#     InstanceId = var.bastion_host_id
#   }
# }

