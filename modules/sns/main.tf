resource "aws_sns_topic" "CPU_Utilization" {
  name       = "bastion-host-cpu-alarm"
  fifo_topic = false
}

resource "aws_sns_topic_subscription" "sns-topic" {
  count     = length(var.subscriber_emails)
  topic_arn = aws_sns_topic.CPU_Utilization.arn
  protocol  = "email"
  endpoint  = element(var.subscriber_emails, count.index)
}