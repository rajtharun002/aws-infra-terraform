variable "project-name" {
  description = "Project Name"
  type        = string
  default     = ""
}

variable "subscriber_emails" {
  type = list(string)
  default = [
    "santhanarajdesingurajan@gmail.com"
  ]
}