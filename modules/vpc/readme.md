## REQUIRED VARIABLES

PROJECT-NAME 
REGION-NAME

The variables specified above have the proper place holders and were sent through the main VPC module.

## OPTIONAL VARIABLES

* cidr_block
* public_subnet_cidr_blocks
* private_subnet_cidr_blocks

The variables specified above have the default values. We can configure it as per our requirements

## OUTPUTS

* vpc_id
* public_subnet_01_id
* public_subnet_02_id
* private_subnet_01_id
* private_subnet_02_id

These outputs are produced by this module. Using placeholders, we may use this output in another module.