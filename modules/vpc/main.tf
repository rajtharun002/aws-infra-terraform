#####################################
#Fetch Availability Zones
#####################################

data "aws_availability_zones" "azs" {
  filter {
    name   = "region-name"
    values = [var.region]
  }
}


#####################################
#Define VPC
#####################################

resource "aws_vpc" "vpc" {
  cidr_block           = var.cidr_block
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "${var.project-name}-VPC"
  }
}

#####################################
#Define Public Subnets
#####################################

resource "aws_subnet" "public_subnet_01" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.public_subnet_cidr_blocks, 0)
  availability_zone = element(data.aws_availability_zones.azs.names, 0)

  map_public_ip_on_launch = true

  tags = {
    Name                                                   = "${var.project-name}-public-subnet-01",
    "kubernetes.io/role/elb"                               = 1,
    "kubernetes.io/cluster/sutherland-poc-terraform" = "shared"
  }
}
resource "aws_subnet" "public_subnet_02" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = element(var.public_subnet_cidr_blocks, 1)
  availability_zone       = element(data.aws_availability_zones.azs.names, 1)
  map_public_ip_on_launch = true

  tags = {
    Name                                                   = "${var.project-name}-public-subnet-02",
    "kubernetes.io/role/elb"                               = "1",
    "kubernetes.io/cluster/sutherland-poc-terraform" = "shared"
  }
}

#####################################
#Define Private Subnets
#####################################

resource "aws_subnet" "private_subnet_01" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.private_subnet_cidr_blocks, 0)
  availability_zone = element(data.aws_availability_zones.azs.names, 0)

  tags = {
    Name                                                   = "${var.project-name}-private-subnet-01",
    "kubernetes.io/role/internal-elb"                      = "1",
    "kubernetes.io/cluster/sutherland-poc-terraform" = "shared"
  }
}

resource "aws_subnet" "private_subnet_02" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.private_subnet_cidr_blocks, 1)
  availability_zone = element(data.aws_availability_zones.azs.names, 1)

  tags = {
    Name                                                   = "${var.project-name}-private-subnet-02",
    "kubernetes.io/role/internal-elb"                      = "1",
    "kubernetes.io/cluster/sutherland-poc-terraform" = "shared"
  }
}

#####################################
#Define Internet Gateway
#####################################

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project-name}-IGW"
  }
}

#####################################
#Define Elastic IP
#####################################

resource "aws_eip" "nat_eip" {
  vpc = true
  tags = {
    "Name" = "${var.project-name}-NAT-EIP"
  }
}

#####################################
#Define NAT Gateway
#####################################

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnet_01.id

  tags = {
    Name = "${var.project-name}-NGW"
  }
}


#####################################
#Define Public Route Table
#####################################

resource "aws_route_table" "public_RT" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project-name}-public-RT"
  }
}

resource "aws_route" "public_route" {
  route_table_id         = aws_route_table.public_RT.id
  destination_cidr_block = var.anywhere_cidr
  gateway_id             = aws_internet_gateway.igw.id
}


#########################################
#Define Private Route Table 
#########################################

resource "aws_route_table" "private_RT" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project-name}-private-RT"
  }
}

resource "aws_route" "private_route" {
  route_table_id         = aws_route_table.private_RT.id
  destination_cidr_block = var.anywhere_cidr
  nat_gateway_id         = aws_nat_gateway.ngw.id

}

#######################################
#Define Public Route Table Association
#######################################

resource "aws_route_table_association" "public_RT_association_01" {
  subnet_id      = aws_subnet.public_subnet_01.id
  route_table_id = aws_route_table.public_RT.id

}

resource "aws_route_table_association" "public_RT_association_02" {
  subnet_id      = aws_subnet.public_subnet_02.id
  route_table_id = aws_route_table.public_RT.id

}

#######################################
#Define Private Route Table Association
#######################################

resource "aws_route_table_association" "private_RT_association_01" {
  subnet_id      = aws_subnet.private_subnet_01.id
  route_table_id = aws_route_table.private_RT.id
}

resource "aws_route_table_association" "private_RT_association_02" {
  subnet_id      = aws_subnet.private_subnet_02.id
  route_table_id = aws_route_table.private_RT.id
}

#####################################
# Security Group
#####################################

resource "aws_security_group" "sg" {
  vpc_id = aws_vpc.vpc.id

  ingress {
    description = "SSH"
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = [var.office_public_IP]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.anywhere_cidr]
  }

  tags = {
    Name = "${var.project-name}-custom-sg"
  }
}



