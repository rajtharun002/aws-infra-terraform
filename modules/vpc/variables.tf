variable "project-name" {
  description = "Project Name"
  type        = string
  default     = ""
}

variable "region" {
  description = "Project infra region"
  type        = string
  default     = ""
}

variable "cidr_block" {
  description = "VPC cidr block"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr_blocks" {
  description = "Public subnet cidr blocks"
  type        = list(any)
  default     = ["10.0.0.0/24", "10.0.1.0/24"]
}

variable "private_subnet_cidr_blocks" {
  description = "Private cidr blocks"
  type        = list(any)
  default     = ["10.0.2.0/24", "10.0.3.0/24"]
}

variable "anywhere_cidr" {
  type    = string
  default = "0.0.0.0/0"
}

variable "office_public_IP" {
  type    = string
  default = "49.207.186.69/32"
}

variable "ssh_port" {
  type    = number
  default = 22
}
