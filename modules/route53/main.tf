#####################################
# Create Route53 Zone
#####################################

resource "aws_route53_zone" "prod" {
  name = "doodlebluepoc.cloud"

  tags = {
    Environment = "prod"
  }
}

#####################################
# Create Route53 Records
#####################################

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.prod.zone_id
  name    = "preprod.doodlebluepoc.cloud"
  type    = "CNAME"
  ttl     = 300
  records = [var.project-name]
}

