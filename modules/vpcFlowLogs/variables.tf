variable "project-name" {
  description = "Project Name"
  type        = string
  default     = ""
}

variable "vpc_id" {
  type = string
}