resource "aws_cloudwatch_log_group" "vpc_flow_logs_log_group" {
  retention_in_days = 14
  name              = "${var.project-name}-vpc-flow-logs"
}

#####################################
#Define Role for cloudwatch
#####################################

resource "aws_iam_role" "cloudwatch-access-role" {
  name = "${var.project-name}-cloudwatch-access-role"

  assume_role_policy = file("./policies/vpc_flow_logs_assume_role_policy.json")
}

#########################################
#Define IAM Policy for access cloudwatch
#########################################

resource "aws_iam_role_policy" "cloudwatch-access-policy" {
  name = "${var.project-name}-cloudwatch-access-policy"
  role = aws_iam_role.cloudwatch-access-role.id

  policy = file("./policies/cloud_watch_access_policy.json")
}



#####################################
#Define vpc flow log
#####################################

resource "aws_flow_log" "vpc_flow_log" {
  iam_role_arn    = aws_iam_role.cloudwatch-access-role.arn
  log_destination = aws_cloudwatch_log_group.vpc_flow_logs_log_group.arn
  traffic_type    = "ALL"
  vpc_id          = var.vpc_id
  tags = {
    "Name" = "${var.project-name}-vpc-flowlog"
  }
}
