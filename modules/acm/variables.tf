variable "domain_name" {
  type    = string
  default = "doodlebluepoc.cloud"
}

variable "project-name" {
  description = "Project Name"
  type        = string
  default     = ""
}
