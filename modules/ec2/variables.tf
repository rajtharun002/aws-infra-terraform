variable "project-name" {
  description = "Project Name"
  type        = string
  default     = ""
}


variable "bastion_instance_type" {
  description = "Bastion instance type"
  type        = string
  default     = "t3a.large"
}

variable "office_public_IP" {
  type    = string
  default = "49.207.186.69/32"
}

variable "bastion_key_name" {
  description = "Bastion Pem Key name"
  type        = string
  default     = "multicoreware_poc_key"
}

variable "anywhere_cidr" {
  type    = string
  default = "0.0.0.0/0"
}

variable "public_subnet_01_id" {
  default = ""
}

variable "public_subnet_02_id" {
  default = ""
}

variable "private_subnet_01_id" {
  default = ""
}

variable "private_subnet_02_id" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

variable "ssh_port" {
  type    = number
  default = 22
}

variable "jenkins_port" {
  type    = number
  default = 8080
}

variable "ami_filter" {
  type    = string
  default = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
}

variable "volume_type" {
  type    = string
  default = "gp2"
}

variable "volume_size" {
  type    = number
  default = 50
}

variable "owner_id" {
  type        = string
  default     = "099720109477"
  description = "List of AMI owners to limit search. ex: amazon, aws-marketplace, microsoft"
}


variable "multicoreware-bastion-eip-allocation_id" {
  type    = string
  default = "eipalloc-09ebd73c7b0a0bca1"
}