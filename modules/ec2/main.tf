#####################################
# Fetch AMI ID
#####################################

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_filter]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = [var.owner_id]
}


#####################################
# Security Group
#####################################

resource "aws_security_group" "bastion_sg" {
  vpc_id = var.vpc_id

  ingress {
    description = "SSH"
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = [var.anywhere_cidr]
  }

  ingress {
    description = "Jenkins"
    from_port   = var.jenkins_port
    to_port     = var.jenkins_port
    protocol    = "tcp"
    cidr_blocks = [var.anywhere_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.anywhere_cidr]
  }

  tags = {
    Name = "${var.project-name}-bastion-sg"
  }
}

#####################################
# EIP for instance
#####################################

# resource "aws_eip" "bastion_eip" {
#   vpc = true
#   tags = {
#     "Name" = "Bastion EIP"
#   }
# }

data "aws_eip" "by_allocation_id" {
  id = var.multicoreware-bastion-eip-allocation_id
}

#####################################
# Bastion Host Instance
#####################################

resource "aws_instance" "bastion_ec2" {
  subnet_id              = var.public_subnet_01_id
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.bastion_instance_type
  key_name               = var.bastion_key_name
  vpc_security_group_ids = [aws_security_group.bastion_sg.id]

  # ebs_block_device {
  #   device_name = "/dev/xvdb"
  #   volume_type = "gp2"
  #   volume_size = 50
  # }

  root_block_device {
    volume_size = 50
  }

  user_data = file("./scripts/bastion_user_data.sh")

  tags = {
    Name = "${var.project-name}-bastion-host"
  }
}

#####################################
# EIP Association
#####################################

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.bastion_ec2.id
  allocation_id = var.multicoreware-bastion-eip-allocation_id
}


