output "basiton_IP" {
  value = data.aws_eip.by_allocation_id.public_ip
}

output "bastion_security_group_id" {
  value = aws_security_group.bastion_sg.id
}

output "bastion_host_endpoint" {
  value = aws_instance.bastion_ec2.public_dns
}

output "bastion_host_id" {
  value = aws_instance.bastion_ec2.id
}