# EKS controlplane variables
variable "project-name" {
  description = "Project Name"
  type        = string
  default     = ""
}

variable "private_subnet_01_id" {
  description = "Private subnet 1"
  type        = string
}

variable "private_subnet_02_id" {
  description = "Private subnet 2"
  type        = string
}

# EKS controlplane variables
variable "eks_cluster_version" {
  description = "EKS cluster version to be created"
  type        = string
  default     = "1.24"
}

# EKS node group variables
variable "node_group_name" {
  description = "EKS node group name"
  type        = string
  default     = "EKS-nodegroup-name"
}

variable "node_group_desired_size" {
  description = "desired number of worker nodes"
  type        = number
  default     = 2
}

variable "node_group_min_size" {
  description = "minimum number of worker nodes"
  type        = number
  default     = 1
}

variable "node_group_max_size" {
  description = "maximum number of worker nodes"
  type        = number
  default     = 2
}

variable "node_group_max_unavailable" {
  description = "maximum number of worker nodes can be unavailable during update"
  type        = number
  default     = 2
}

variable "node_group_ami_type" {
  description = "worker nodes AMI type"
  type        = string
  default     = "AL2_x86_64"
}

variable "node_group_capacity_type" {
  description = "worker nodes capacity type"
  type        = string
  default     = "ON_DEMAND"
}

variable "node_group_disk_size" {
  description = "worker nodes disk size"
  type        = number
  default     = 20
}

variable "node_group_instance_types" {
  description = "worker nodes instance type"
  type        = list(string)
  default     = ["t3.medium"]
}

variable "node_group_ec2_ssh_key" {
  description = "worker nodes SSH key-pair name"
  type        = string
  default     = "suthernland_poc_key"
}

# EKS OIDC ROOT CA Thumbprint - valid until 2037
variable "eks_oidc_root_ca_thumbprint" {
  type        = string
  description = "Thumbprint of Root CA for EKS OIDC, Valid until 2037"
  default     = "9e99a48a9960b14926bb7f3b02e22da2b0ab7280"
}