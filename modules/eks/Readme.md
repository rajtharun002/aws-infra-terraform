Expected input variables
````````````````````
#EKS variables
1.project_name
2.eks_cluster_version
3.private_subnet_01_id
4.private_subnet_02_id

#Node group variables
1.node_group_name
2.node_group_desired_size
3.node_group_min_size
4.node_group_max_size
5.node_group_max_unavailable
6.node_group_ami_type
7.node_group_capacity_type
8.node_group_disk_size
9.node_group_instance_types
10.node_group_ec2_ssh_key

Output this module provides
```````````````````````````
1.cluster_id
2.cluster_arn
3.cluster_certificate_authority_data
4.cluster_endpoint
5.cluster_version
6.cluster_iam_role_arn
7.cluster_oidc_issuer_url
8.cluster_primary_security_group_id
9.node_group_private_id
10.node_group_private_status
11.node_group_private_version