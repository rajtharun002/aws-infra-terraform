resource "aws_iam_role" "eks_cluster_master_iam_role" {
  name = "${var.project-name}-eks-cluster-master-iam-role"

  assume_role_policy = file("./policies/eks_cluster_master_role_policy.json")
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_cluster_master_iam_role.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.eks_cluster_master_iam_role.name
}