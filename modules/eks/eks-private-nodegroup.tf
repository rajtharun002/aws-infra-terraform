resource "aws_eks_node_group" "eks_private_nodegroup" {
  cluster_name    = aws_eks_cluster.eks_cluster.name
  node_group_name = var.project-name
  version         = var.eks_cluster_version
  node_role_arn   = aws_iam_role.eks_nodegroup_role.arn
  subnet_ids      = [var.private_subnet_01_id, var.private_subnet_02_id]

  ami_type       = var.node_group_ami_type
  capacity_type  = var.node_group_capacity_type
  disk_size      = var.node_group_disk_size
  instance_types = var.node_group_instance_types


  remote_access {
    ec2_ssh_key = var.node_group_ec2_ssh_key
  }

  scaling_config {
    desired_size = var.node_group_desired_size
    max_size     = var.node_group_max_size
    min_size     = var.node_group_min_size
  }

  update_config {
    max_unavailable = var.node_group_max_unavailable
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]

  tags = {
    Name = "${var.project-name}-EKS-node-group-tag"
  }

}
