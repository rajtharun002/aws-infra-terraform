# # CREATE IAM USER

# data "aws_caller_identity" "current" {}
# output "account_id" {
#   value = data.aws_caller_identity.current.account_id
# }


# resource "aws_iam_user" "admin_user" {
#   name = "${var.project_name}-eksadmin1"
#   path = "/"
#   force_destroy = true
# }

# # ATTACH ADMININISTRATOR POLICY TO USER
# resource "aws_iam_user_policy_attachment" "admin_user" {
#   user       = aws_iam_user.admin_user.name
#   policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
# }

# resource "aws_iam_user" "basic_user" {
#   name = "${var.project_name}-eksadmin2"
#   path = "/"
#   force_destroy = true
# }

# resource "aws_iam_user_policy" "basic_user_eks_policy" {
#   name = "${var.project_name}-eks-full-access-policy"
#   user = aws_iam_user.basic_user.name

#   # Terraform's "jsonencode" function converts a
#   # Terraform expression result to valid JSON syntax.
#   policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = [
#           "iam:ListRoles",
#           "eks:*",
#           "ssm:GetParameter"
#         ]
#         Effect   = "Allow"
#         Resource = "*"
#       },
#     ]
#   })
# }


