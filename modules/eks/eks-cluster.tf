resource "aws_eks_cluster" "eks_cluster" {
  name     = var.project-name
  role_arn = aws_iam_role.eks_cluster_master_iam_role.arn
  version  = var.eks_cluster_version

  vpc_config {
    subnet_ids = [var.private_subnet_01_id, var.private_subnet_02_id]
    endpoint_public_access = true
    public_access_cidrs    = ["0.0.0.0/0"]
  }

  kubernetes_network_config {
    service_ipv4_cidr = "172.20.0.0/16"
  }

  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.AmazonEKSVPCResourceController,
  ]
}
