#####################################
#Define S3 Bucket
#####################################

resource "aws_s3_bucket" "private-bucket" {
  bucket = "${var.project-name}-preprod"

  tags = {
    Name        = "${var.project-name}-preprod"
    Environment = "Dev"
  }
}

#####################################
#Define Bucket ACL
#####################################

resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.private-bucket.id
  acl    = "private"
}

#####################################
#Define Bucket Lifecycle rule
#####################################

# resource "aws_s3_bucket_lifecycle_configuration" "bucket_lifecycle" {
#   bucket = aws_s3_bucket.private-bucket.id

#   rule {
#     id     = "my-lifecycle-rule"
#     status = "Enabled"

#     expiration {
#       days = 90
#     }

#     transition {
#       days          = 30
#       storage_class = "STANDARD_IA"
#     }

#     transition {
#       days          = 60
#       storage_class = "GLACIER"
#     }

#   }
# }
