output "terraform_eks_codebuild_kubectl_role" {
  value = aws_iam_role.terraform_eks_codebuild_kubectl_role.arn
}

# output "clone_url" {
#   value = aws_codecommit_repository.booking-app.clone_url_http
# }