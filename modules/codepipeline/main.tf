# AWS CODEBUILD ROLE
resource "aws_iam_role" "terraform_codebuild_role" {
  name = "${var.project-name}-codebuild-role"

  assume_role_policy = file("./policies/code_build_assume_role_policy.json")

}

resource "aws_iam_role_policy" "terraform_codebuild_policy" {
  role = aws_iam_role.terraform_codebuild_role.name
  name = "${var.project-name}-codebuild-policy"

  policy = file("./policies/code_build_policy.json")
}

# eks-codebuild-sts-assume-role
resource "aws_iam_policy" "eks-codebuild-sts-assume-role-policy" {
  name   = "${var.project-name}-eks-codebuild-sts-assume-role-policy"
  path   = "/"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "${aws_iam_role.terraform_eks_codebuild_kubectl_role.arn}"
        }
    ]
}
POLICY
}


resource "aws_iam_role_policy_attachment" "eks-codebuild-sts-assume-role-policy" {
  role       = aws_iam_role.terraform_codebuild_role.name
  policy_arn = aws_iam_policy.eks-codebuild-sts-assume-role-policy.arn
}

# AmazonEC2ContainerRegistryFullAccess
resource "aws_iam_role_policy_attachment" "attach_ecr_full_access" {
  role       = aws_iam_role.terraform_codebuild_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess"
}

data "aws_caller_identity" "current" {}

resource "aws_iam_role" "terraform_eks_codebuild_kubectl_role" {
  name = "${var.project-name}-eks-code-build-kubectl-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ecs_describe_policy" {
  role = aws_iam_role.terraform_eks_codebuild_kubectl_role.name
  name = "${var.project-name}-eks-describe-policy"

  policy = file("./policies/eks_describe_policy.json")
}

resource "aws_iam_role" "codepipeline_role" {
  name = "${var.project-name}-codepipeline-role"

  assume_role_policy = file("./policies/code_pipeline_assume_role_policy.json")

}

resource "aws_iam_role_policy" "codepipeline_policy" {
  name = "${var.project-name}-codepipeline-policy"
  role = aws_iam_role.codepipeline_role.id

  policy = file("./policies/code_pipeline_policy.json")
}



