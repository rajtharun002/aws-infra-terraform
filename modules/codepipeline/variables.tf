variable "project-name" {
  description = "Project Name"
  type        = string
  default     = ""
}

variable "repository_branch" {
  default = "demo"
}

variable "eks_cluster_name" {
  default = ""
}