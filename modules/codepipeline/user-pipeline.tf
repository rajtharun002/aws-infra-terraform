# # # AWS CODECOMMIT REPOSITORY
# resource "aws_codecommit_repository" "user-app" {
#   repository_name = "${var.project-name}-user-app"
#   description     = "My CodeCommit Repository"
# }

# # # -------------<<<<<< ####### CODEBUILD ####### >>>>>>>>>>>-----------------------#

# resource "aws_s3_bucket" "codebuild-s3-bucket-user-app" {
#   bucket = "${var.project-name}-codebuild-user-app-${data.aws_caller_identity.current.account_id}"
#   #   acl    = "private"
#   force_destroy = true

# }

# resource "aws_ecr_repository" "terraform-user-app" {
#   name                 = "${var.project-name}-user-app"
#   image_tag_mutability = "MUTABLE"
#   force_delete         = true

#   image_scanning_configuration {
#     scan_on_push = true
#   }
# }

# resource "aws_codebuild_project" "terraform-build-project-user-app" {
#   name          = "${var.project-name}-build-project-user-app"
#   description   = "build project"
#   build_timeout = "60"
#   service_role  = aws_iam_role.terraform_codebuild_role.arn

#   artifacts {
#     type = "NO_ARTIFACTS"
#   }

#   cache {
#     type     = "S3"
#     location = aws_s3_bucket.codebuild-s3-bucket-user-app.arn
#   }

#   environment {
#     compute_type                = "BUILD_GENERAL1_SMALL"
#     image                       = "aws/codebuild/standard:6.0"
#     type                        = "LINUX_CONTAINER"
#     image_pull_credentials_type = "CODEBUILD"
#     privileged_mode             = true

#     environment_variable {
#       name  = "REPOSITORY_URI"
#       value = aws_ecr_repository.terraform-user-app.repository_url
#     }

#     environment_variable {
#       name  = "REPOSITORY_NAME"
#       value = aws_ecr_repository.terraform-user-app.name
#     }

#     environment_variable {
#       name  = "REPOSITORY_BRANCH"
#       value = var.repository_branch
#     }

#     environment_variable {
#       name  = "EKS_CLUSTER_NAME"
#       value = var.eks_cluster_name
#     }

#     environment_variable {
#       name  = "EKS_KUBECTL_ROLE_ARN"
#       value = aws_iam_role.terraform_eks_codebuild_kubectl_role.arn
#     }


#   }

#   logs_config {
#     cloudwatch_logs {
#       group_name  = "log-group"
#       stream_name = "log-stream"
#     }

#     s3_logs {
#       status   = "ENABLED"
#       location = "${aws_s3_bucket.codebuild-s3-bucket-user-app.id}/build-log"
#     }
#   }

#   source {
#     type            = "CODECOMMIT"
#     location        = aws_codecommit_repository.user-app.clone_url_http
#     git_clone_depth = 1
#   }

#   source_version = "refs/heads/master"
# }
# # # -------------------------<<<<<>>>>>>>>>>--------------------


# resource "aws_s3_bucket" "codepipeline-bucket-user-app" {
#   bucket = "${var.project-name}-codepipeline-user-app-${data.aws_caller_identity.current.account_id}"
#   #   acl    = "private"
#   force_destroy = true
# }


# resource "aws_codepipeline" "codepipeline-user-app" {
#   name     = "${var.project-name}-pipeline-user-app"
#   role_arn = aws_iam_role.codepipeline_role.arn

#   artifact_store {
#     location = aws_s3_bucket.codepipeline-bucket-user-app.bucket
#     type     = "S3"
#   }

#   stage {
#     name = "Source"

#     action {
#       name             = "SourceAction"
#       category         = "Source"
#       owner            = "AWS"
#       provider         = "CodeCommit"
#       version          = "1"
#       output_artifacts = ["source_output"]

#       configuration = {
#         RepositoryName = aws_codecommit_repository.user-app.repository_name
#         BranchName     = var.repository_branch
#       }
#       run_order = 1
#     }
#   }

#   stage {
#     name = "Build"

#     action {
#       name             = "BuildAction"
#       category         = "Build"
#       owner            = "AWS"
#       provider         = "CodeBuild"
#       input_artifacts  = ["source_output"]
#       output_artifacts = ["build_output"]
#       version          = "1"

#       configuration = {
#         ProjectName = aws_codebuild_project.terraform-build-project-user-app.name
#       }
#       run_order = 1
#     }
#   }
# }

