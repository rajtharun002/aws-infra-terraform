# # AWS CODECOMMIT REPOSITORY
resource "aws_codecommit_repository" "booking-app" {
  repository_name = "${var.project-name}-booking-app"
  description     = "My CodeCommit Repository"
}

# # -------------<<<<<< ####### CODEBUILD ####### >>>>>>>>>>>-----------------------#

resource "aws_s3_bucket" "codebuild-s3-bucket-booking-app" {
  bucket = "${var.project-name}-codebuild-booking-app-${data.aws_caller_identity.current.account_id}"
  #   acl    = "private"
  force_destroy = true

}

resource "aws_ecr_repository" "terraform-booking-app" {
  name                 = "${var.project-name}-booking-app"
  image_tag_mutability = "MUTABLE"
  force_delete         = true

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_codebuild_project" "terraform-build-project-booking-app" {
  name          = "${var.project-name}-build-project-booking-app"
  description   = "build project"
  build_timeout = "60"
  service_role  = aws_iam_role.terraform_codebuild_role.arn

  artifacts {
    type = "CODEPIPELINE"
  }

  cache {
    type     = "S3"
    location = aws_s3_bucket.codebuild-s3-bucket-booking-app.arn
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:6.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = true

    environment_variable {
      name  = "REPOSITORY_URI"
      value = aws_ecr_repository.terraform-booking-app.repository_url
    }

    environment_variable {
      name  = "REPOSITORY_NAME"
      value = aws_ecr_repository.terraform-booking-app.name
    }

    environment_variable {
      name  = "REPOSITORY_BRANCH"
      value = var.repository_branch
    }

    environment_variable {
      name  = "EKS_CLUSTER_NAME"
      value = var.eks_cluster_name
    }

    environment_variable {
      name  = "EKS_KUBECTL_ROLE_ARN"
      value = aws_iam_role.terraform_eks_codebuild_kubectl_role.arn
    }

    environment_variable {
      name = "TELEGRAM_TOKEN"
      value = "6250788980:AAFbW4sfMJQVVzpNDkUJU4RDglES00Id9eI"
    }

    environment_variable {
      name = "CHAT_ID"
      value = "-896692489"
    }

  }

  logs_config {
    cloudwatch_logs {
      group_name  = "log-group"
      stream_name = "log-stream"
    }

    s3_logs {
      status   = "ENABLED"
      location = "${aws_s3_bucket.codebuild-s3-bucket-booking-app.id}/build-log"
    }
  }

  source {
    type            = "CODEPIPELINE"
    location        = aws_codecommit_repository.booking-app.clone_url_http
    git_clone_depth = 1
    buildspec       = "buildspec.yml"
  }

  source_version = "refs/heads/master"
}

resource "aws_codebuild_project" "terraform-build-project-booking-app-deploy" {
  name          = "${var.project-name}-build-project-booking-app-deploy"
  description   = "build project deploy"
  build_timeout = "60"
  service_role  = aws_iam_role.terraform_codebuild_role.arn
  
  artifacts {
    type = "CODEPIPELINE"
  }

  cache {
    type     = "S3"
    location = aws_s3_bucket.codebuild-s3-bucket-booking-app.arn
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:6.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = true

    environment_variable {
      name  = "REPOSITORY_URI"
      value = aws_ecr_repository.terraform-booking-app.repository_url
    }

    environment_variable {
      name  = "REPOSITORY_NAME"
      value = aws_ecr_repository.terraform-booking-app.name
    }

    environment_variable {
      name  = "REPOSITORY_BRANCH"
      value = var.repository_branch
    }

    environment_variable {
      name  = "EKS_CLUSTER_NAME"
      value = var.eks_cluster_name
    }

    environment_variable {
      name  = "EKS_KUBECTL_ROLE_ARN"
      value = aws_iam_role.terraform_eks_codebuild_kubectl_role.arn
    }

    environment_variable {
      name = "TELEGRAM_TOKEN"
      value = "6250788980:AAFbW4sfMJQVVzpNDkUJU4RDglES00Id9eI"
    }

    environment_variable {
      name = "CHAT_ID"
      value = "-896692489"
    }

  }



  logs_config {
    cloudwatch_logs {
      group_name  = "log-group"
      stream_name = "log-stream"
    }

    s3_logs {
      status   = "ENABLED"
      location = "${aws_s3_bucket.codebuild-s3-bucket-booking-app.id}/build-log"
    }
  }

  source {
    type            = "CODEPIPELINE"
    # location        = aws_codecommit_repository.booking-app.clone_url_http
    git_clone_depth = 1
    buildspec = "deploy-buildspec.yml" 
  }

  source_version = "refs/heads/master"
}
# # -------------------------<<<<<>>>>>>>>>>--------------------


resource "aws_s3_bucket" "codepipeline-bucket-booking-app" {
  bucket = "${var.project-name}-codepipeline-booking-app-${data.aws_caller_identity.current.account_id}"
  #   acl    = "private"
  force_destroy = true
}


resource "aws_codepipeline" "codepipeline-booking-app" {
  name     = "${var.project-name}-pipeline-booking-app"
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline-bucket-booking-app.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "SourceAction"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        RepositoryName = aws_codecommit_repository.booking-app.repository_name
        BranchName     = var.repository_branch
      }
      # run_order = 1
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_output"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.terraform-build-project-booking-app.name
        # BuildSpec   = "buildspec.yml"
      }
      # run_order = 1
    }
  }

  stage {
    name = "Deploy"

    action {
      name             = "Deploy"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["deploy_output"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.terraform-build-project-booking-app-deploy.name
        # PrimarySource = "source_output"
        # BuildSpec = "deploy-buildspec.yml"


      }
      # run_order = 1
    }
  }
  
}

